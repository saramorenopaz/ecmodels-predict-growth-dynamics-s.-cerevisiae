# ecModels predict growth dynamics S. cerevisiae

Supplementary material for Enzyme-constrained models predict the dynamics of Saccharomyces cerevisiae growth in continuous, batch and fed-batch bioreactors.

Yeast8 and ecYeast8 models present in the Models directory were obtained from: Lu H, Li F, Sánchez BJ, et al. A consensus S. cerevisiae metabolic model Yeast8 and its ecosystem for comprehensively probing cellular metabolism. Nat Commun. 2019;10(1):3586. doi:10.1038/s41467-019-11581-3
